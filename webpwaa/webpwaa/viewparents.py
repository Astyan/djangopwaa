from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.views.generic import CreateView
from rest_framework.exceptions import PermissionDenied
from collections import OrderedDict
from operator import attrgetter


def get_names_list(obj, order_by, reverse):
    fields_name = []
    wanted_fields = obj.list_wanted_fields()
    for field in obj._meta.get_fields():
        field_values = field.__dict__
        for key in wanted_fields:
            try:
                if field_values["name"] == key:
                    if field_values["name"] == order_by:
                        reverse = (reverse+1)%2
                        fields_name.append((reverse, key, field_values["verbose_name"]))
                    else:
                        fields_name.append((0, key, field_values["verbose_name"]))
            except KeyError:
                pass
    return fields_name

def context_transformation(object_list):
    data = []
    for obj in object_list:
        data.append(OrderedDict())
        data[-1]["pk"] =  obj.pk
        data[-1]["fields"] =  OrderedDict()
        for key in obj.list_wanted_fields():
            #handle the manytomany
            try:
                elems = eval("obj."+key+".all()")
                data[-1]["fields"][key] = '-'.join([elem.__str__() for elem in elems])
            #handle when the key is not a database object
            except AttributeError:
                data[-1]["fields"][key] = eval("obj."+key)
    return data

class WebPWAAListView(ListView):
    template_name = ""
    paginate_by = 10
    name = ""
    detailurl = ""
    pageurl = ""
    default_order_by = ""
    default_reverse = 0
    user_specific = False
    
    def get_queryset(self):
        order_by = self.request.GET.get("order_by", self.default_order_by)
        reverse = self.request.GET.get("reverse", self.default_reverse) 
        db_request = self.model.objects.all().order_by(order_by)
        if int(reverse) == 1:
            db_request = db_request.reverse()
        if self.user_specific and self.request.user.is_authenticated():
            db_request = db_request.filter(user=self.request.user)
        return db_request
    
        
    def get_context_data(self, **kwargs):
        context = super(WebPWAAListView, self).get_context_data(**kwargs)
        #set values
        order_by = self.request.GET.get("order_by", self.default_order_by)
        reverse = self.request.GET.get("reverse", self.default_reverse)
        context["page"] = self.request.GET.get("page", 1)
        context["reverse"] = reverse
        #need to check if not empty and raise a Emptyerror if it is
        #in the template need to handle this error too
        #params to test (good order)
        try:
            context["params"] = context["object_list"][0].list_wanted_fields()
            context["sorting"] = context["object_list"][0].sorted_wanted_fields()
            #params to print
            context["printedparams"] = get_names_list(context["object_list"][0], 
                                                      order_by,
                                                      int(reverse))
            #object we want
            context["object_list"] = context_transformation(context["object_list"])
        except IndexError:
            context["params"] = None
            context["printedparams"] = None
            context["object_list"] = None
            context["sorting"] = None
        #other info that cannot be generated
        context["name"] = self.name
        context["detailurl"] = self.detailurl
        context["pageurl"] = self.pageurl
        context["extraurl"] = "order_by="+order_by+"&reverse="+str(reverse)
        return context

class WebPWAADetailView(DetailView):
    template_name = ""
    model_name = ""
    name = ""
    pk = None

    def test_perm(self, obj):
        """ do the permission"""
        try:
            objuser = obj.user
        except Exception: 
            return 
        if objuser != self.request.user:
            raise PermissionDenied()

    def get_object(self):
        obj = super(WebPWAADetailView, self).get_object()
        self.test_perm(obj)
        self.pk = obj.pk
        d_obj = OrderedDict()
        #to use the verbose_name
        d_name = {}
        for key in self.model._meta.__dict__["_forward_fields_map"]:
            d_name[key] = self.model._meta.__dict__["_forward_fields_map"][key].verbose_name
        #fill the data
        for key in obj.detail_wanted_fields():
            #handle database object
            try:
                databaseobjects = eval("obj."+key+".all()")
                d_obj[d_name[key]] = '-'.join([elem.__str__() for elem in databaseobjects])
            #handle when the key is not a database object
            except AttributeError:
                d_obj[d_name[key]] = eval('obj.'+key)        
        return d_obj
    
    def get_context_data(self, **kwargs):
        context = super(WebPWAADetailView, self).get_context_data(**kwargs)
        context["pk"] = self.pk
        context["name"] = self.name
        return context
        
class CreateViewURL(CreateView):
    model = None
    template_name = ''
    form_class = None
    form_url = ''
    name = ''
    
    def get_context_data(self, **kwargs):
        context = super(CreateViewURL, self).get_context_data(**kwargs)
        context['formurl'] = self.form_url
        context['name'] = self.name
        return context

