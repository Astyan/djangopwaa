from django import forms
from learning.models import Machine

class MachineForm(forms.ModelForm):
    pass
    
    class Meta:
        model = Machine
        fields = ['machine_model', 'name', 'csvfile', 'mgffile', 
                  'nb_peaks_max', 'nb_paths_max' , 'ppm', 'aas',
                  'protease']

    def __init__(self, *args, **kwargs):
        """ """
        super(MachineForm, self).__init__(*args, **kwargs)
        self.fields['machine_model'].widget.attrs.update({'class' : 'form-control'})
        self.fields['name'].widget.attrs.update({'class' : 'form-control'})
        self.fields['nb_peaks_max'].widget.attrs.update({'class' : 'form-control'})
        self.fields['nb_paths_max'].widget.attrs.update({'class' : 'form-control'})
        self.fields['ppm'].widget.attrs.update({'class' : 'form-control'})
        self.fields['aas'].widget.attrs.update({'class' : 'form-control'})
        self.fields['protease'].widget.attrs.update({'class' : 'form-control'})
    
    def save(self, force_insert=False, force_update=False, commit=True):
        m = super(MachineForm, self).save(commit=commit)
        m.state = 'pending'
        if commit:
            m.save()
        return m
