from __future__ import unicode_literals

from django.db import models
from chimicals.models import (AminoAcid, Protease, PTM)
#~ from picklefield.fields import PickledObjectField


# Create your models here.

class CutWeight(models.Model):
    
    n_term = models.FloatField(null=True, verbose_name="N-term")
    c_term = models.FloatField(null=True, verbose_name="C-term")
    c_cut = models.FloatField(null=True, verbose_name="C-cut")
    b_cut = models.FloatField(null=True, verbose_name="B-cut")
    a_cut = models.FloatField(null=True, verbose_name="A-cut")
    x_cut = models.FloatField(null=True, verbose_name="X-cut")
    y_cut = models.FloatField(null=True, verbose_name="Y-cut")
    z_cut = models.FloatField(null=True, verbose_name="Z-cut")
    none = models.FloatField(null=True, verbose_name="None")
    
    def __unicode__(self):
        return str(self.id)

class EdgeWeight(models.Model):
    
    charge_diff = models.FloatField(null=True, verbose_name="Charge Difference Weights")
    immonium_found = models.FloatField(null=True, verbose_name="Immonium Found Weights")
    protease_break = models.FloatField(null=True, verbose_name="Protease Breaks Weights")
    
    def __unicode__(self):
        return str(self.id)
    
class PeakWeight(models.Model):
    
    cut = models.FloatField(null=True, verbose_name="Cut Weights")
    rmz = models.FloatField(null=True, verbose_name="RMZ Weights")
    symetric = models.FloatField(null=True, verbose_name="Symetric Weights")
    knowed_charge = models.FloatField(null=True, verbose_name="Knowed Charge Weights")
    intensity = models.FloatField(null=True, verbose_name="Intensity Weights")
    other_peaks = models.FloatField(null=True, verbose_name="Other Peaks")

    def __unicode__(self):
        return str(self.id)

class Machine(models.Model):
    MACHINE_STATES = (
        ('pending', 'Pending'),
        ('running', 'Running'),
        ('finished', 'Finished'),
        ('aborted', 'Aborted')
    )
    
    
    machine_model = models.CharField(verbose_name = "Model",
                                     max_length = 255)
    name = models.CharField(verbose_name = "Machine Name",
                            max_length = 255,
                            unique=True)
    cutweight = models.ForeignKey(CutWeight, related_name="cutweight", verbose_name="Cut Weight", null=True)
    edgeweight = models.ForeignKey(EdgeWeight, verbose_name="Edge Weight", null=True)
    peakweight = models.ForeignKey(PeakWeight, related_name="peakweight", verbose_name="Peak Weight", null=True)
    peakselection = models.ForeignKey(PeakWeight, related_name="peakselection", verbose_name="Peak Selection", null=True)
    cutselection = models.ForeignKey(CutWeight, related_name="cutselection", verbose_name="Cut Selection", null=True)
    state = models.CharField(verbose_name = "State", 
                             max_length=100, 
                             choices=MACHINE_STATES,
                             default='pending')
    csvfile = models.FileField(verbose_name = "CSV Data", upload_to='uploads')
    mgffile = models.FileField(verbose_name = "MGF Data", upload_to='uploads')
    #analysisparameter like
    nb_peaks_max = models.PositiveSmallIntegerField(verbose_name = "Max Peaks Per Graph")
    nb_paths_max = models.PositiveSmallIntegerField(verbose_name = "Max Paths Per Graph")
    ppm = models.PositiveSmallIntegerField(verbose_name = "PPM")
    aas = models.ManyToManyField(AminoAcid,
                                 related_name="%(class)s_aas",
                                 verbose_name = "Amino Acids")
    protease = models.ForeignKey(Protease,
                                 related_name="%(class)s_protease",
                                 verbose_name = "Protease", null=True)
    #~ massbinpos = PickledObjectField(verbose_name = "Mass Possiblility", null=True)
    
    
    def __unicode__(self):
        return str(self.name)



    
    
    
