from __future__ import absolute_import
from celery import shared_task

from django.conf import settings

from learning.models import Machine, CutWeight, EdgeWeight, PeakWeight
from chimicals.models import AminoAcid, Link, SmallResidue, IsoAtom

from pymultievolve.algorithm import GeneticAlgorithm
from pymultievolve.reduction import SelTournamentNoRemise

from pwaa.learning.genetic_learning import (EvaluationGlobal,
                                            ParameterPopulation, 
                                            CrossOver, 
                                            Mutation,
                                            create_all_valid_graph,
                                            create_all_simple_gaph)
                                            
from pwaa.basicknowledge.melemscreate import fill_all_aa, fill_all_link, fill_all_residue, fill_all_ptm
from pwaa.utils.atoms import MAtomsMassOp, MAtoms
from pwaa.analyser.relative_amount import RelativeAmount
from pwaa.analyser.mass_search import MassPossibleDic


def cut_to_db(cut):
    dres = {}
    for key in cut:
        new_key = key.lower()
        new_key = list(new_key)
        if new_key[1] == '-':
            new_key[1] = '_'
        new_key = ''.join(new_key)
        dres[new_key] = cut[key]
    return dres
    
def get_massbinpos(machineid):
    m = Machine.objects.get(id=machineid)
    atoms = {}
    for atom in IsoAtom.objects.all():
        atoms.update(atom.as_dict())
    #create aaptm
    #with all the aaptm knowed ?Right know with all the ones we want
    aas = {}
    for aa in m.aas.all():
        aas.update(aa.as_dict())
    aas = fill_all_aa(allaa=aas)
    #compute the constraintes
    matoms = MAtoms(atoms=atoms)
    ra = RelativeAmount(matoms.get_mono_masses_dictatoms(), aas)
    ra.construct_atoms_list()
    ra.construct_matrix()
    ra.fill_matrix()
    ra.relation_tab()
    #compute the possibility
    mpd = MassPossibleDic(m.ppm, 0.0, ra.rt, 0.0, 600.0, 2, 
                          atoms=atoms, celery=True)
    mpd.create_jobs(settings.PWAA_NB_PROCESS) 
    return mpd.result
    
@shared_task
def new_machine(machineid):
    massbinpos = get_massbinpos(machineid)
    #create atoms
    m = Machine.objects.get(id=machineid)
    m.state = "running"
    m.save()
    atoms = {}
    for atom in IsoAtom.objects.all():
        atoms.update(atom.as_dict())
    mamo = MAtomsMassOp(atoms)
    #create aas ptms and aaptms
    aas = {}
    for aa in m.aas.all():
        aas.update(aa.as_dict())
    aas = fill_all_aa(allaa=aas)
    links = []
    for link in Link.objects.all():
        links.append(link.as_dict())
    links = fill_all_link(alllinks=links)
    #create residue
    residues = {}
    for residue in SmallResidue.objects.all():
        residues.update(residue.as_dict())
    all_residues = fill_all_residue(allresiduecom=residues)
    #create the protease mass
    #get all the protease
    proteasecuts = []
    protease = m.protease.as_dict()
    for aa in aas:
        for k, v in protease.items():
            for aasymbol in v:
                if aa.aa == aasymbol:
                    proteasecuts.append(mamo.atomlist_to_masses(aa.composition, "mono"))
    #do this algorithme
    openedcsvfile = m.csvfile
    openedmgffile = m.mgffile
    #create the graph
    valid_graphs = create_all_simple_gaph(openedcsvfile, openedmgffile, links, atoms=atoms)
    population = ParameterPopulation()
    poptosend = population.run(500)
    #~ evaluation = EvaluationGlobal(valid_graphs)
    evaluation = EvaluationGlobal(valid_graphs, aas, links, 
                                  all_residues, proteasecuts=proteasecuts, 
                                  atoms=atoms, nb_peaks=m.nb_peaks_max,
                                  nb_paths=m.nb_paths_max, 
                                  massposbins=massbinpos)
    evaluation.open_csv(openedcsvfile, to_open=False)
    reduction = SelTournamentNoRemise(goal="minimize", tournsize=7)
    crossover = CrossOver(indcross=0.3)
    mutation = Mutation(indpb=0.1)
    ga = GeneticAlgorithm(mutation=mutation, crossover=crossover, 
                          reduction=reduction, evaluation=evaluation, 
                          population=poptosend, population_size=500, nb_generation=40,
                          mutation_probability=0.05, crossover_probability=0.2,
                          nb_process=settings.PWAA_NB_PROCESS, 
                          do_stats=True, celery=True)
    ga.run()
    #result of the ga
    best_dna = ga.get_best_individual().dna
    cw = cut_to_db(best_dna["cutpeak"])
    ew = best_dna["edgeweight"]
    pw = best_dna["peakweight"]
    ps = best_dna["peakselection"]
    cs = cut_to_db(best_dna["cutselection"])
    #get save the results
    cwd = CutWeight(**cw)
    cwd.save()
    ewd = EdgeWeight(None, *ew)
    ewd.save()
    pwd = PeakWeight(None, *pw)
    pwd.save()
    csd = CutWeight(**cs)
    csd.save()
    psd = PeakWeight(None, *ps)
    psd.save()
    
    #put the result in cleaned_data to save it afterward
    m.cutweight = cwd
    m.edgeweight = ewd
    m.peakweight = pwd
    m.cutselection = csd
    m.peakselection = psd
    m.state = "finished"
    m.save()
