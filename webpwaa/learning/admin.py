from django.contrib import admin

from learning.models import Machine, CutWeight, EdgeWeight, PeakWeight
# Register your models here.

admin.site.register(Machine)
admin.site.register(CutWeight)
admin.site.register(EdgeWeight)
admin.site.register(PeakWeight)
