import copy

from django.shortcuts import render
from webpwaa.viewparents import (WebPWAAListView, WebPWAADetailView, 
                                 CreateViewURL)
from learning.models import Machine, CutWeight, EdgeWeight, PeakWeight
from learning.form import MachineForm
from django.views.generic import DetailView, CreateView
from django.core.urlresolvers import reverse_lazy
from django.http import HttpResponseRedirect, HttpResponse
from learning.tasks import new_machine
# Create your views here.

#~ def new_machine(usefull_args, formcopy):
    #~ openedcsvfile = usefull_args["files"]["csvfile"]
    #~ openedmgffile = usefull_args["files"]["mgffile"]
    #do the analysis
    #~ cw = {"n_term" : 0.2, "c_term" : 0, "c_cut" : 0, "b_cut" : 0,
    #~ "a_cut" : 0, "x_cut" : 0, "y_cut" : 0, "z_cut" : 0}
    #~ ew = {"charge_diff" : 0, "immonium_found" : 0, "protease_break": 0}
    #~ pw = {"cut" : 0, "rmz" : 0, "symetric" : 0, "knowed_charge" : 0,
            #~ "intensity" : 0, "other_peaks" : 0}
    #get save the results
    #~ cwd = CutWeight(**cw)
    #~ cwd.save()
    #~ ewd = EdgeWeight(**ew)
    #~ ewd.save()
    #~ pwd = PeakWeight(**pw)
    #~ pwd.save()
    #~ #put the result in cleaned_data to save it afterward
    #~ formcopy.__dict__["cleaned_data"]["cutweight"] = cwd
    #~ formcopy.__dict__["cleaned_data"]["edgeweight"] = ewd
    #~ formcopy.__dict__["cleaned_data"]["peakweight"] = pwd
    #~ formcopy.save()


class NewMachine(CreateView):
    model = Machine
    template_name = 'learning/import.html'
    form_class = MachineForm
    form_url = 'new_machine'
    name = 'New Machine' #why it don't work
    success_url=reverse_lazy('learning_index')
    
    def form_valid(self, form):
        #create the object to put in a celery queue
        objres = form.save()
        #send the object in the queue
        new_machine.delay(objres.id)
        return HttpResponseRedirect(reverse_lazy('learning_index'))
    
    def get_context_data(self, **kwargs):
        context = super(NewMachine, self).get_context_data(**kwargs)
        context['formurl'] = self.form_url
        #changing fields
        #~ print context
        return context
