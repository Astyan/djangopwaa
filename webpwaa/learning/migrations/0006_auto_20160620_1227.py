# -*- coding: utf-8 -*-
# Generated by Django 1.9.5 on 2016-06-20 12:27
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('chimicals', '0014_auto_20160608_1410'),
        ('learning', '0005_auto_20160617_0826'),
    ]

    operations = [
        migrations.AddField(
            model_name='machine',
            name='aas',
            field=models.ManyToManyField(related_name='machine_aas', to='chimicals.AminoAcid', verbose_name='Amino Acids'),
        ),
        migrations.AddField(
            model_name='machine',
            name='nb_paths_max',
            field=models.PositiveSmallIntegerField(default=1000, verbose_name='Max Paths Per Graph'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='machine',
            name='nb_peaks_max',
            field=models.PositiveSmallIntegerField(default=100, verbose_name='Max Peaks Per Graph'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='machine',
            name='ppm',
            field=models.PositiveSmallIntegerField(default=50, verbose_name='PPM'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='machine',
            name='protease',
            field=models.ManyToManyField(related_name='machine_protease', to='chimicals.Protease', verbose_name='Protease'),
        ),
        migrations.AddField(
            model_name='machine',
            name='ptms',
            field=models.ManyToManyField(related_name='machine_ptms', to='chimicals.PTM', verbose_name='PTM'),
        ),
    ]
