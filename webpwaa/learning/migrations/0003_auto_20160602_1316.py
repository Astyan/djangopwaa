# -*- coding: utf-8 -*-
# Generated by Django 1.9.5 on 2016-06-02 13:16
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('learning', '0002_auto_20160527_1218'),
    ]

    operations = [
        migrations.AddField(
            model_name='machine',
            name='cutselection',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='cutselection', to='learning.CutWeight', verbose_name='Cut Selection'),
        ),
        migrations.AddField(
            model_name='machine',
            name='peakselection',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='peakselection', to='learning.PeakWeight', verbose_name='Peak Selection'),
        ),
        migrations.AlterField(
            model_name='machine',
            name='cutweight',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='cutweight', to='learning.CutWeight', verbose_name='Cut Weight'),
        ),
        migrations.AlterField(
            model_name='machine',
            name='peakweight',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='peakweight', to='learning.PeakWeight', verbose_name='Peak Weight'),
        ),
    ]
