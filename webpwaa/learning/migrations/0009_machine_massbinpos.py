# -*- coding: utf-8 -*-
# Generated by Django 1.9.5 on 2016-06-24 08:46
from __future__ import unicode_literals

from django.db import migrations
import picklefield.fields


class Migration(migrations.Migration):

    dependencies = [
        ('learning', '0008_remove_machine_ptms'),
    ]

    operations = [
        migrations.AddField(
            model_name='machine',
            name='massbinpos',
            field=picklefield.fields.PickledObjectField(editable=False, null=True, verbose_name='Mass Possiblility'),
        ),
    ]
