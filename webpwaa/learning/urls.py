from django.conf.urls import patterns, url
from django.views.generic import TemplateView
from django.core.urlresolvers import reverse_lazy
from learning.views import NewMachine


urlpatterns = [
    url(r'^$', TemplateView.as_view(template_name="learning_base.html"), name="learning_index"),
    url(r'new_learning$', NewMachine.as_view(), name="new_learning"),
]
