from django import forms
from django.db.models import Q

from analysis.models import Analysis, AnalysisParam, MSMSAnalysis
from analysis.tasks import save_prot_and_score


class AnalysisParamForm(forms.ModelForm):
    
    class Meta:
        model = AnalysisParam
        fields = ["name", "ppm", "aas", "protease", "ptms", 
                  "nb_peaks_max", "nb_paths_max", "machine"]
    
    def __init__(self, *args, **kwargs):
        """ """
        super(AnalysisParamForm, self).__init__(*args, **kwargs)
        self.fields['name'].widget.attrs.update({'class' : 'form-control'})
        self.fields['ppm'].widget.attrs.update({'class' : 'form-control'})
        self.fields['aas'].widget.attrs.update({'class' : 'form-control'})
        self.fields['protease'].widget.attrs.update({'class' : 'form-control'})
        self.fields['ptms'].widget.attrs.update({'class' : 'form-control'})
        self.fields['nb_peaks_max'].widget.attrs.update({'class' : 'form-control'})
        self.fields['nb_paths_max'].widget.attrs.update({'class' : 'form-control'})
        self.fields['machine'].widget.attrs.update({'class' : 'form-control'})
        
class AnalysisParamFormEdit(AnalysisParamForm):
    
    class Meta:
        model = AnalysisParam
        fields = [ "name", "ppm", "activate", "aas", "protease", "ptms" ]    

    
class AnalysisForm(forms.ModelForm):
    
    class Meta:
        model = Analysis
        fields = [ "name", "mgffile", "comment", "analysisparam" ]
        widgets = {
            "comment": forms.Textarea(attrs={'rows' : 10}),
        }
    
    def __init__(self, *args, **kwargs):
        """ """
        super(AnalysisForm, self).__init__(*args, **kwargs)
        self.fields['name'].widget.attrs.update({'class' : 'form-control'})
        self.fields['comment'].widget.attrs.update({'class' : 'form-control'})
        self.fields['analysisparam'].widget.attrs.update({'class' : 'form-control'})
        #show only analysisparam that have finish there computation
        self.fields['analysisparam'].widget.choices.queryset = AnalysisParam.objects.filter(activate=True)        
        
    def is_valid(self):
        valid = super(AnalysisForm, self).is_valid()
        if not valid:
            return False
        analysisparam = AnalysisParam.objects.get(Q(pk=self.cleaned_data["analysisparam"]) |
                                                  Q(name=self.cleaned_data["analysisparam"]))  
        if analysisparam.activate == False:
            self._errors['analysisparam_not_activated'] = "The analysis parameter is not activate"
            return False
        return True
        
        
        
class AnalysisFormEdit(forms.ModelForm):
    
    class Meta:
        model = Analysis
        fields = [ "name", "comment", "note" ]   
        widgets = {
            "comment": forms.Textarea(attrs={'rows' : 10}),
        } 

    def __init__(self, *args, **kwargs):
        """ """
        super(AnalysisFormEdit, self).__init__(*args, **kwargs)
        self.fields['name'].widget.attrs.update({'class' : 'form-control'})
        self.fields['comment'].widget.attrs.update({'class' : 'form-control'})
        self.fields['note'].widget.attrs.update({'class' : 'form-control'})
    
class BlastForm(forms.Form):
    database = forms.ChoiceField(choices = (("uniprotkb", "uniprotkb"),
                                            ("uniprotkb_swissprot", "uniprotkb_swissprot")))
    
    def __init__(self, *args, **kwargs):
        super(BlastForm, self).__init__(*args, **kwargs)
        self.fields["database"].widget.attrs.update({'class' : 'form-control'})
    
    def do_blast(self, analysis_id):
        save_prot_and_score.delay(analysis_id, self.cleaned_data["database"]) 
    
