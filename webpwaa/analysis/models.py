#-*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import User
from picklefield.fields import PickledObjectField

from chimicals.models import (AminoAcid, Protease, PTM)
from learning.models import Machine


# Create your models here.
            
    
class AnalysisParam(models.Model):
    """ """
    name = models.CharField(verbose_name = "Name",
                            max_length = 255,
                            unique=True)
    ppm = models.PositiveSmallIntegerField(verbose_name = "PPM")
    aas = models.ManyToManyField(AminoAcid,
                                 related_name="%(class)s_aas",
                                 verbose_name = "Amino Acids")
    protease = models.ForeignKey(Protease,
                                 related_name="%(class)s_protease",
                                 verbose_name = "Protease")
    ptms = models.ManyToManyField(PTM,
                                  related_name="%(class)s_ptms",
                                  verbose_name = "PTM")
    activate = models.BooleanField(verbose_name = "Activate", default=False) #change for different state and avaible only if the state is finish
    machine = models.ForeignKey(Machine, null=True, verbose_name = "Machine")
    algoversion = models.CharField(null=True, verbose_name = "Algorithm Version",
                                   max_length = 255)
    nb_peaks_max = models.PositiveSmallIntegerField(verbose_name = "Max Peaks Per Graph")
    nb_paths_max = models.PositiveSmallIntegerField(verbose_name = "Max Paths Per Graph")
    user = models.ForeignKey(User, null=True, verbose_name = "User")
    massbinpos = PickledObjectField(verbose_name = "Mass Possiblility", null=True)
    
    def list_wanted_fields(self):
        return ["name", "ppm", "protease", "activate", "machine", "aas", "ptms" ]
    
    def sorted_wanted_fields(self):
        return ["name", "ppm"]
    
    def detail_wanted_fields(self):
        return self.list_wanted_fields()+ ["nb_peaks_max", "nb_paths_max"]
    
    def __unicode__(self):
        return self.name
    
class Analysis(models.Model):
    """ """
    ANALYSIS_STATES = (
        ('pending', 'Pending'),
        ('running', 'Running'),
        ('finished', 'Finished'),
        ('aborted', 'Aborted'),
        ('blast', 'Blast')
    )
    
    name = models.CharField(verbose_name = "Name",
                            max_length = 255,
                            unique = True)
    user = models.ForeignKey(User, verbose_name = "User")
    mgffile = models.FileField(verbose_name = "MGF", upload_to='uploads')
    date = models.DateField(auto_now=True,
                            verbose_name = "Date")
    state = models.CharField(verbose_name = "State", 
                             max_length=100, 
                             choices=ANALYSIS_STATES,
                             default='pending')
    identification = models.CharField(null=True, verbose_name = "Protein Identified",
                                      max_length = 255)
    score = models.FloatField(null=True, verbose_name = "Global Score")
    note = models.SmallIntegerField(null=True, verbose_name = "User Note")
    comment = models.CharField(verbose_name = "Comment",
                               max_length = 1024)
    analysisparam = models.ForeignKey(AnalysisParam, null=True, verbose_name = "Analysis Parameter")

    def list_wanted_fields(self):
        return ["name", "date", "state", "identification", "score", "note", "analysisparam"]
    
    def detail_wanted_fields(self):
        return self.list_wanted_fields()+["comment"]+["mgffile"]
    
    def sorted_wanted_fields(self):
        return ["name", "date", "state", "identification", "score", "note", "analysisparam"]
        
    def __unicode__(self):
        return self.name
    
class MSMSAnalysis(models.Model):
    """ """
    name = models.CharField(verbose_name = "Name",
                            max_length = 1024)
    hashname = models.CharField(verbose_name = "Id",
                                max_length = 1024)
    analysis = models.ForeignKey(Analysis, verbose_name = "Analysis")
    score = models.FloatField(verbose_name = "MSMS Score")
    sequencefound = models.CharField(verbose_name = "Sequence",
                                     max_length = 10240,
                                     null = True,
                                     default = None)
    #adding the machine
    
    def list_wanted_fields(self):
        return ["name", "hashname", "score", "sequencefound"]
    
    def sorted_wanted_fields(self):
        return ["name", "score", "sequencefound"]
    
    def __unicode__(self):
        return self.name


class SequenceElem(models.Model):
    """ """
    
    msmsanalysis = models.ForeignKey(MSMSAnalysis)
    mass = models.FloatField(verbose_name = "Mass",
                             null = True,
                             default = None)
    amino_acid = models.ForeignKey(AminoAcid,
                                   verbose_name = "Amino Acid",
                                   null = True,
                                   default=None)
    ptm = models.ForeignKey(PTM, null=True, 
                            verbose_name = "PTM", default=None)
    
    class Meta:
        order_with_respect_to = 'msmsanalysis'
    
    
    def __unicode__(self):
        res = ""
        if self.mass != None:
            res = str(self.mass)
        else:
            res = str(self.amino_acid.symbol)
            if self.ptm != None:
                res += "-"+str(self.ptm.name) 
        return res
        
        
    
    
    
    
    
        
    
    

