from django.contrib import admin

from analysis.models import Analysis, AnalysisParam, MSMSAnalysis, SequenceElem
# Register your models here.

class AnalysisParamAdmin(admin.ModelAdmin):
    list_display = ('name', 'ppm', 'algoversion', 'activate', 'user', 'massbinpos')
    list_filter = ('name', 'ppm', 'algoversion', 'activate', 'user')
    ordering = ('name', 'ppm', 'algoversion', 'activate', 'user' )
    search_fields = ('name', 'ppm', 'algoversion', 'activate', 'user')

class AnalysisAdmin(admin.ModelAdmin):
    list_display = ('name', 'user', 'mgffile', 'date', 'state', 
                    'identification', 'score', 'note', 'comment',
                    'analysisparam')
    list_filter = ('name', 'user', 'mgffile', 'date', 'state', 
                   'identification', 'score', 'note', 'comment',
                   'analysisparam')
    ordering = ('name', 'user', 'mgffile', 'date', 'state', 
                'identification', 'score', 'note', 'comment',
                'analysisparam')
    search_fields = ('name', 'user', 'mgffile', 'date', 'state', 
                     'identification', 'score', 'note', 'comment',
                     'analysisparam')

class MSMSAnalysisAdmin(admin.ModelAdmin):
    list_display = ('name', 'analysis', 'score', 'sequencefound')
    list_filter = ('name', 'analysis', 'score', 'sequencefound')
    ordering = ('name', 'analysis', 'score', 'sequencefound')
    search_fields = ('name', 'analysis', 'score', 'sequencefound')


admin.site.register(Analysis, AnalysisAdmin)
admin.site.register(AnalysisParam, AnalysisParamAdmin)
admin.site.register(MSMSAnalysis, MSMSAnalysisAdmin)
admin.site.register(SequenceElem)
