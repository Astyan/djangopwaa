# -*- coding: utf-8 -*-
# Generated by Django 1.9.5 on 2016-06-07 12:33
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('analysis', '0008_auto_20160607_0831'),
    ]

    operations = [
        migrations.AlterField(
            model_name='msmsanalysis',
            name='name',
            field=models.CharField(max_length=1024, verbose_name='Name'),
        ),
    ]
