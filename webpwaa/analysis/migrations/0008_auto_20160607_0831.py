# -*- coding: utf-8 -*-
# Generated by Django 1.9.5 on 2016-06-07 08:31
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('analysis', '0007_auto_20160602_1316'),
    ]

    operations = [
        migrations.AlterField(
            model_name='analysis',
            name='date',
            field=models.DateField(auto_now=True, verbose_name='Date'),
        ),
    ]
