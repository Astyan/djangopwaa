from django.shortcuts import render

from django.views.generic import CreateView, DeleteView, UpdateView, FormView
from django.core.urlresolvers import reverse_lazy
from django.http import HttpResponseRedirect
# Create your views here.

from webpwaa.viewparents import (WebPWAAListView, WebPWAADetailView, 
                                 CreateViewURL)
from analysis.models import AnalysisParam, Analysis, MSMSAnalysis
from analysis.forms import (AnalysisParamForm, AnalysisParamFormEdit, 
                            AnalysisForm, AnalysisFormEdit, BlastForm)
from analysis.tasks import new_analysis, new_analysisparam


#Analysis
class NewAnalysis(CreateViewURL):
    model = Analysis
    template_name = 'analysis/new_analysis.html'
    form_class = AnalysisForm
    form_url = 'new_analysis'
    name = 'New Analysis'
    success_url = 'analysis_list'

    def form_valid(self, form):
        obj = form.save(commit=False)
        obj.user = self.request.user
        obj.save()
        new_analysis.delay(obj.id)
        return HttpResponseRedirect(reverse_lazy(self.success_url))

class AnalysisListView(WebPWAAListView):
    model = Analysis
    name = 'Analysis'
    template_name = 'analysis/analysis_list.html'
    detailurl = 'analysis_detail'
    pageurl = 'analysis_list'
    default_order_by = 'date'
    default_reverse = 1
    user_specific = True
    
class AnalysisDetailView(WebPWAADetailView):
    model = Analysis
    modelname = 'analysis'
    name = 'Analysis'
    template_name = 'analysis/analysis_detail.html'
    
    def get_context_data(self, **kwargs):
        context = super(AnalysisDetailView, self).get_context_data(**kwargs)
        context["delete"] = "analysis_delete"
        context["modify"] = "analysis_update"
        context["msms"] = "msms_list"
        context["parameter"] = "analysis_param_detail"
        context["blast"] = "blast"
        context["paramid"] = AnalysisParam.objects.filter(name=context["object"][u'Analysis Parameter'])[0].id
        context["download"] = "download"
        return context

class AnalysisUpdateView(UpdateView):
    model = Analysis
    name = "Update Analysis"
    template_name = 'analysis/edit_analysis.html'
    form_class = AnalysisFormEdit
    
    def get_success_url(self):
        return reverse_lazy('analysis_update', kwargs=self.kwargs)

class AnalysisDeleteView(DeleteView):
    model = Analysis
    name = "Delete Analysis"
    template_name = 'analysis/default_delete.html'
    success_url = reverse_lazy('analysis_list')
    
    def get_context_data(self, **kwargs):
        context = super(AnalysisDeleteView, self).get_context_data(**kwargs)
        context["name"] = self.name
        return context

#analysis params
class NewAnalysisParam(CreateViewURL):
    model = AnalysisParam
    template_name = 'analysis/new_analysis.html'
    form_class = AnalysisParamForm
    form_url = 'new_analysis_params'
    name = 'New Analysis Parameter'
    success_url = 'analysis_params_list'
    
    def form_valid(self, form):
        super(NewAnalysisParam, self).form_valid(form) #to save the whole stuff
        #save the user
        obj = form.save(commit=False)
        obj.user = self.request.user
        obj.save()
        #send the object to celery
        new_analysisparam.delay(obj.id)
        #redirect to the success url
        return HttpResponseRedirect(reverse_lazy(self.success_url))

        
class AnalysisParamListView(WebPWAAListView):
    model = AnalysisParam
    name = 'Analysis Parameters'
    template_name = 'analysis/analysis_list.html'
    detailurl = 'analysis_param_detail'
    pageurl = 'analysis_params_list'
    default_order_by = 'name'
    user_specific = True

class AnalysisParamDetailView(WebPWAADetailView):
    model = AnalysisParam
    modelname = 'analysisparam'
    name = 'Analysis Parameters'
    template_name = 'analysis/analysis_param_detail.html'
    
    def get_context_data(self, **kwargs):
        context = super(AnalysisParamDetailView, self).get_context_data(**kwargs)
        context["delete"] = "analysis_param_delete"
        context["modify"] = "analysis_param_update"
        if len(Analysis.objects.filter(analysisparam=self.pk)) == 0:
            context["notlinked"] = True
        else:
            context["notlinked"] = False
        return context
    
class AnalysisParamDeleteView(DeleteView):
    model = AnalysisParam
    name = "Delete Analysis Parameter"
    template_name = 'analysis/default_delete.html'
    success_url = reverse_lazy('analysis_params_list')
    
    def get_context_data(self, **kwargs):
        context = super(AnalysisParamDeleteView, self).get_context_data(**kwargs)
        context["name"] = self.name
        return context

class AnalysisParamUpdateView(UpdateView):
    model = AnalysisParam
    name = "Update Analysis Parameter"
    template_name = 'analysis/edit_analysis.html'
    form_class = AnalysisParamFormEdit
    
    def get_success_url(self):
        return reverse_lazy('analysis_param_detail', kwargs=self.kwargs)

#MSMS
class MSMSAnalysisListView(WebPWAAListView):
    model = MSMSAnalysis
    name = 'MS/MS Analysis'
    template_name = 'analysis/msms_list.html'
    detailurl = 'msms_detail'
    pageurl = 'msms_list'
    default_order_by = 'score'
    default_reverse = 1
    user_specific = False
    
    def get_queryset(self):
        #the function to apply on the reduce
        def best_score(a, b):
            if a.score > b.score:
                return a
            else:
                return b
        #call to the parent and do the operation
        qset = super(MSMSAnalysisListView, self).get_queryset()
        if len(qset) == 0:
            return qset
        #get the name with the good order (set cannot work
        names = []
        for elem in qset:
            if elem.name not in names:
                names.append(elem.name)
        #give the ones to print
        res = []
        for name in names:
            elems = []
            for elemqset in qset:
                if elemqset.name == name:
                    elems.append(elemqset)
            res.append(reduce(best_score, elems))
        return res 

class MSMSAnalysisDetailView(WebPWAAListView):
    model = MSMSAnalysis
    name = 'MS/MS Detail'
    template_name = 'analysis/msms_detail.html'
    detailurl = ''
    pageurl = 'msms_detail'
    default_order_by = 'score'
    default_reverse = 1
    user_specific = False
    
    def get_queryset(self):
        qset = super(MSMSAnalysisDetailView, self).get_queryset()
        qset = qset.filter(hashname=self.kwargs["id"])
        return qset

#new blast
class BlastView(FormView):
    form_class = BlastForm
    template_name = 'analysis/blast_analysis.html'
    success_url= 'analysis_list'
    
    def form_valid(self, form):
        form.do_blast(self.kwargs["pk"])
        return HttpResponseRedirect(reverse_lazy(self.success_url))
        
    
    
    
