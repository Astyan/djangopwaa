from django.conf.urls import patterns, url
from django.views.generic import TemplateView
from django.core.urlresolvers import reverse_lazy


from analysis.views import (NewAnalysis, 
                            AnalysisListView, 
                            NewAnalysisParam,
                            AnalysisDetailView,
                            AnalysisUpdateView,
                            AnalysisDeleteView,
                            AnalysisParamListView,
                            AnalysisParamDetailView, 
                            AnalysisParamDeleteView,
                            AnalysisParamUpdateView,
                            MSMSAnalysisListView,
                            MSMSAnalysisDetailView,
                            BlastView)

urlpatterns = [ url(r'^analysis_list$', AnalysisListView.as_view(),
                    name="analysis_list"),
                url(r'^new_analysis$', NewAnalysis.as_view(),
                    name="new_analysis"),
                url(r'^analysis_detail/(?P<pk>\d+)$', AnalysisDetailView.as_view(),
                    name="analysis_detail"),
                url(r'^analysis_delete/(?P<pk>\d+)$', AnalysisDeleteView.as_view(),
                    name="analysis_delete"),
                url(r'^analysis_update/(?P<pk>\d+)$', AnalysisUpdateView.as_view(),
                    name="analysis_update"),
                
                url(r'^analysis_params_list$', AnalysisParamListView.as_view(),
                    name="analysis_params_list"),    
                url(r'new_analysis_params$', NewAnalysisParam.as_view(),
                    name="new_analysis_params"),
                url(r'^analysis_param_detail/(?P<pk>\d+)$', AnalysisParamDetailView.as_view(),
                    name="analysis_param_detail"),
                url(r'^analysis_param_delete/(?P<pk>\d+)$', AnalysisParamDeleteView.as_view(),
                    name="analysis_param_delete"),
                url(r'^analysis_param_update/(?P<pk>\d+)$', AnalysisParamUpdateView.as_view(),
                    name="analysis_param_update"),
                
                url(r'^msms_list/(?P<pk>\d+)$', MSMSAnalysisListView.as_view(),
                    name="msms_list"),
                url(r'^msms_detail/(?P<id>\w+)$', MSMSAnalysisDetailView.as_view(),
                    name="msms_detail"),
                url(r'blast/(?P<pk>\d+)$', BlastView.as_view(), name="blast"),
                
                url(r'^$', TemplateView.as_view(template_name="analysis_base.html"), name="analysis_index"),
]
