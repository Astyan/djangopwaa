from __future__ import absolute_import
from celery import shared_task
import pickle
import ctypes

from django.conf import settings

from analysis.models import Analysis, MSMSAnalysis, AnalysisParam, SequenceElem
from learning.models import Machine, CutWeight, EdgeWeight, PeakWeight
from chimicals.models import (Atom, IsoAtom, AminoAcid, Protease, Link,
                              Position, PTM, SmallResidue)

from pwaa.analyser.mass_elems import masses_bins_elems
from pwaa.analyser.mztom import MassSpectrum
from pwaa.analyser.valid_graph import ValidGraph
from pwaa.basicknowledge.melemscreate import fill_all_aa, fill_all_ptm, fill_all_link, fill_all_residue
from pwaa.basicknowledge.mcombielemscreate import reason_all_aaptm, reason_all_doublelinks, reason_all_aaptmresidue
from pwaa.analyser.enrich_graph import EnrichGraph, WeightGraph
from pwaa.evaluation.subsequence_probability import EdgeWeight, PeakWeight
from pwaa.basicknowledge.mcombielemsoperation import LinksOperations
from pwaa.analyser.theorical_spectrum import TheoricalSpectrum
from pwaa.evaluation.matching_spectrum import MatchingSpectrum
from pwaa.analyser.mass_search import MassPossibleDic
from pwaa.analyser.relative_amount import RelativeAmount
from pwaa.utils.atoms import MAtoms, MAtomsMassOp


from multiprocesspool.multiprocesspool import MultiProcessPool
from multiblast.multiblast import all_hits


def sequences_translation(sequences):
    tsequences = []
    for sequence in sequences:
        tsequence = ""
        for elem in sequence:
            tsequence = tsequence + elem.aa
        tsequences.append(tsequence)
    return tsequences

def sequence_str(sequence):
    seqstr = ""
    for elem in sequence:
        seqstr += str(elem)+" "
    return seqstr
    
@shared_task
def new_analysis(analysis_id):
    #all the init
    analysis = Analysis.objects.get(id=analysis_id)
    analysis.state = "running"
    analysis.save()
    #~ massposbins = pickle.load(open("massbinposs", "rb"))
    #get the analysisparam
    analysisparam = analysis.analysisparam
    massposbins = analysisparam.massbinpos 
    #
    atoms={}
    for atom in IsoAtom.objects.all():
        atoms.update(atom.as_dict())
    mamo = MAtomsMassOp(atoms)
    #create selected elems
    aas = {}
    for aa in analysisparam.aas.all():
        aas.update(aa.as_dict())
    aas = fill_all_aa(allaa=aas)
    ptms = {}
    for ptm in analysisparam.ptms.all():
        ptms.update(ptm.as_dict_ptm())
    ptmspos = {}
    for ptm in analysisparam.ptms.all():
        ptmspos.update(ptm.as_dict_ptmpos())
    ptms = fill_all_ptm(allptmpos=ptmspos, allptmcompo=ptms)
    aaptms = reason_all_aaptm(aas, ptms)
    #create atoms
    atoms = {}
    for atom in IsoAtom.objects.all():
        atoms.update(atom.as_dict())
    #create massbinselems
    mbinelems = masses_bins_elems(aaptms, 1, 0.01) #adding ptm and len == 1
    mass_spectrum = MassSpectrum(analysis.mgffile ,to_open=False, atoms=atoms)
    #create links
    links = []
    for link in Link.objects.all():
        links.append(link.as_dict())
    links = fill_all_link(links)
    dlinks = reason_all_doublelinks(links)
    lop = LinksOperations(links)
    #get the proteasecut 
    proteasecuts = []
    protease = analysisparam.protease.as_dict()
    for aa in aas:
        for k, v in protease.items():
            for aasymbol in v:
                if aa.aa == aasymbol:
                    proteasecuts.append(mamo.atomlist_to_masses(aa.composition, "mono"))
    #get the immonium
    residues = {}
    for residue in SmallResidue.objects.all():
        residues.update(residue.as_dict())
    all_residues = fill_all_residue(allresiduecom=residues)
    residues = reason_all_aaptmresidue(aaptms, all_residues)
    #get the machine
    machine = analysisparam.machine
    #get the datas
    edgeweight = machine.edgeweight
    peakweight = machine.peakweight
    ew = EdgeWeight()
    ew.edgeweight(edgeweight.charge_diff, 
                  edgeweight.immonium_found, 
                  edgeweight.protease_break)
    cutweight = machine.cutweight
    cutweight = {"n-term": cutweight.n_term, 
                 "c-term" : cutweight.c_term,
                 "c-cut" : cutweight.c_cut,
                 "b-cut" : cutweight.b_cut,
                 "a-cut" : cutweight.a_cut,
                 "x-cut" : cutweight.x_cut,
                 "y-cut" : cutweight.y_cut,
                 "z-cut" : cutweight.z_cut,
                 "None" : cutweight.none}
    ew.peakweight(peakweight.intensity, peakweight.knowed_charge,
                  peakweight.symetric, peakweight.rmz, 
                  peakweight.cut, cutweight, 
                  peakweight.other_peaks)
    pw = PeakWeight()
    peakselection = machine.peakselection
    cutselection = machine.cutselection
    cutselection = {"n-term": cutselection.n_term, 
                    "c-term" : cutselection.c_term,
                    "c-cut" : cutselection.c_cut,
                    "b-cut" : cutselection.b_cut,
                    "a-cut" : cutselection.a_cut,
                    "x-cut" : cutselection.x_cut,
                    "y-cut" : cutselection.y_cut,
                    "z-cut" : cutselection.z_cut,
                    "None" : cutselection.none}
    pw.peakweight(peakselection.intensity, peakselection.knowed_charge,
                  peakselection.symetric, peakselection.rmz, 
                  peakselection.cut, cutselection, 
                  peakselection.other_peaks)
    #create immonium mass and protease break
    #protease break is simply the amino acid mass
    #immonium is not clear
    lspectrums = []
    for i in range(0, mass_spectrum.get_nb_ions()):
        mass_spectrum.set_ion(i)
        lspectrums.append((mass_spectrum.converted_peak(), 
                           mass_spectrum.get_ion_name()))
    def analysis_and_save(value):
        peaks = value[0]
        spectrum_name = value[1]
        valid_graph = ValidGraph(dlinks, peaks, 600.0, atoms=atoms) #600 da max
        valid_graph.insert_peakgraph_nodes()
        #get only a given number of peaks
        eg = EnrichGraph(valid_graph.get_peakgraph(), analysisparam.ppm, atoms=atoms) 
        eg.enrich_peaks()
        #get the peaks
        wg = WeightGraph(eg.peakgraph, peakweight=pw)
        wg.compute_peak_weight()
        graph = wg.select_best_peak(lim=analysisparam.nb_peaks_max)
        #create the edges
        valid_graph = ValidGraph(dlinks, peaks, 600.0, graph, atoms=atoms)
        valid_graph.insert_edges(massposbins)
        valid_graph.delete_no_path()
        valid_graph.delete_solitary_nodes()
        #select the good edges and path
        eg = EnrichGraph(valid_graph.get_peakgraph(), analysisparam.ppm, atoms=atoms)
        eg.enrich_edges(residues, proteasecuts) #to add the immoniumlist and the proteaselist
        wg = WeightGraph(eg.peakgraph, edgeweight=ew)
        wg.compute_edge_weight()
        graph = wg.select_best_path(lim=analysisparam.nb_paths_max) 
        #get the sequence list 
        valid_graph = ValidGraph(dlinks, peaks, 600.0, graph, atoms=atoms)
        valid_graph.insert_possibility(mbinelems, 0.01, analysisparam.ppm) #50 ppm, step of 0.01
        sequences = valid_graph.get_all_simple_sequences()
        #compute the score for each sequence and save it
        hashname = str(ctypes.c_size_t(hash(str(analysis_id)+spectrum_name)).value)
        # BETTER SAVE OF THE SEQUENCE
        for sequence in sequences:
            ts = TheoricalSpectrum(sequence, lop, atoms=atoms)
            tpeaks = ts.compute_all_peaks()
            ms = MatchingSpectrum(peaks, tpeaks)
            ms.set_max_intensity()
            score = ms.compute_score(analysisparam.ppm)
            #~ sequencefound = sequence_str(sequence)
            msms_analysis = MSMSAnalysis(name=spectrum_name, 
                                         hashname=hashname,
                                         analysis=analysis,
                                         score=score)
            msms_analysis.save()
            for elem in sequence:
                if type(elem) is float:
                    #~ print "float"
                    se = SequenceElem(msmsanalysis=msms_analysis,
                                      mass=elem)
                    se.save()
                else:
                    #~ print "aa"
                    se = SequenceElem(msmsanalysis=msms_analysis,
                                      amino_acid=AminoAcid.objects.get(symbol=elem.aa),
                                      ptm=PTM.objects.get(name=elem.ptm))
                    se.save()
            seqelem_order = msms_analysis.get_sequenceelem_order()
            strseq = ""
            for elem in seqelem_order:
                strseq += str(SequenceElem.objects.get(id=elem))+" "
            msms_analysis.sequencefound = strseq
            msms_analysis.save()
        return 1
    
    def queueop(value, vallistres):
        vallistres.append(value)
        return vallistres
    
    analysis.state = "finished"
    try:
        pp = MultiProcessPool(analysis_and_save, lspectrums, queueop, [], celery=True)
        pp.run(1) # One because of the concurency on the database OperationalError: database is locked
        #should be resolve with other sgbd
    except Exception:    
        analysis.state = "aborted"
    
    analysis.save()
        
@shared_task
def new_analysisparam(analysisparam_id):
    analysisparam = AnalysisParam.objects.get(id=analysisparam_id)
    #create atoms
    atoms = {}
    for atom in IsoAtom.objects.all():
        atoms.update(atom.as_dict())
    #create aaptm
    #with all the aaptm knowed ?Right know with all the ones we want
    aas = {}
    for aa in analysisparam.aas.all():
        aas.update(aa.as_dict())
    aas = fill_all_aa(allaa=aas)
    ptms = {}
    for ptm in analysisparam.ptms.all():
        ptms.update(ptm.as_dict_ptm())
    ptmspos = {}
    for ptm in analysisparam.ptms.all():
        ptmspos.update(ptm.as_dict_ptmpos())
    ptms = fill_all_ptm(allptmpos=ptmspos, allptmcompo=ptms)
    aaptms = reason_all_aaptm(aas, ptms)
    #compute the constraintes
    matoms = MAtoms(atoms=atoms)
    ra = RelativeAmount(matoms.get_mono_masses_dictatoms(), aaptms)
    ra.construct_atoms_list()
    ra.construct_matrix()
    ra.fill_matrix()
    ra.relation_tab()
    #compute the possibility
    mpd = MassPossibleDic(analysisparam.ppm, 0.0, ra.rt, 0.0, 600.0, 2, 
                          atoms=atoms, celery=True)
    mpd.create_jobs(settings.PWAA_NB_PROCESS) 
    result = mpd.result
    analysisparam.massbinpos = result
    analysisparam.activate = True
    analysisparam.save()
    #save the result in a file
                    
@shared_task
def save_prot_and_score(analysis_id, database=None):
    """ """
    if database == None:
        database = "uniprotkb"
    analysis = Analysis.objects.get(id=analysis_id)
    analysis.state = "blast"
    analysis.save()
    email = analysis.user.email
    #get the sequences
    #get the msmsanalysis
    msmsanalysis = MSMSAnalysis.objects.filter(analysis=analysis) #name
    bestanalysis = {}
    for msmsanalyse in msmsanalysis:
        bestanalysis[msmsanalyse.hashname] = msmsanalyse
    for hashname in bestanalysis:
        for msmsanalyse in msmsanalysis:
            if msmsanalyse.hashname == hashname:
                if bestanalysis[hashname].score > msmsanalyse.score:
                    bestanalysis[hashname] = msmsanalyse
    #get the SequenceElem
    sequences = []
    for hashname in bestanalysis:
        seqelem = SequenceElem.objects.filter(msmsanalysis=bestanalysis[hashname])
        sequence = ""
        for elem in seqelem:
            if elem.mass != None:
                sequence += "-"
            else:
                sequence += elem.amino_acid.symbol
        if sequence != '-'*len(sequence):
            sequences.append(sequence)    
    #do the matching
    bscore = 0.
    pname = "No identification"
    if sequences != []:
        res = all_hits(sequences, database, email)
        #save the data
        for key in res:
            if bscore < res[key][score]:
                bscore = res[key][score]
                pname = key
    analysis.identification = pname
    analysis.score = bscore
    analysis.state = "finished"
    analysis.save()

    
    
    
    
    
