from django.conf.urls import patterns, url

from . import views

urlpatterns = [ url(r'^last$', views.show_all_articles, name="all_articles"),
                url(r'^article/(?P<id>\d+)$', views.read_article, name="view_article"),
]
        
