from django.http import HttpResponse, Http404
from django.shortcuts import render, get_object_or_404
from datetime import datetime
from news.models import Article
from django.core.paginator import Paginator, EmptyPage

# Create your views here.


def read_article(request, id):
    article = get_object_or_404(Article, id=id)
    return render(request, 'news/read_article.html', {'article':article})
    

def show_all_articles(request):
    page = request.GET.get('page', 1)
    articles = Article.objects.all()
    articles = articles.order_by("date").reverse() 
    paginator = Paginator(articles, 5)
    try:
        to_show = paginator.page(page)
    except EmptyPage:
        to_show = paginator.page(paginator.num_pages)
        
    return render(request, 'news/show_all_article.html', {'last_articles': to_show,})
