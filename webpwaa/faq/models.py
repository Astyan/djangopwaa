from __future__ import unicode_literals

from django.db import models

# Create your models here.
class Question(models.Model):
    question = models.TextField(null=True)
    answer = models.TextField(null=True)
    
    def __str__(self):
        return self.question
    
