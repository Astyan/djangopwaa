from django.contrib import admin

# Register your models here.
from faq.models import Question

admin.site.register(Question)
