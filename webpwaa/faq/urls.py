from django.conf.urls import patterns, url

from . import views

urlpatterns = [ url(r'^questions$', views.show_all_question, name="questions"),
                url(r'^answer/(?P<id>\d+)$', views.read_answer, name="answer"),
]
