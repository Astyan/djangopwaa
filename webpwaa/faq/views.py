from django.shortcuts import render, get_object_or_404
from datetime import datetime
from faq.models import Question

# Create your views here.

def show_all_question(request):
    questions = Question.objects.all()
    return render(request, 'faq/questions.html', {'questions':questions})
    

def read_answer(request, id):
    answer = get_object_or_404(Question, id=id)
    return render(request, 'faq/read_answer.html', {'answer':answer})
