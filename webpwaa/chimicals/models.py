#-*- coding: utf-8 -*-
from __future__ import unicode_literals
import sys
from django.db import models

# Create your models here.


class DictDictManager(models.Manager):
    method = "as_dict()"
    
    def get_queryset(self):
        qset = super(DictDictManager, self).get_queryset()
        dres = {}
        for obj in qset:
            dres.update(eval("obj."+self.method))
        return dres

class DictListManager(models.Manager):
    method = "as_dict()"
    
    def get_queryset(self):
        qset = super(DictListManager, self).get_queryset()
        lres = [ eval("obj."+self.method) for obj in qset ]
        return lres

class DictPTMManager(DictDictManager):
    method = "as_dict_ptm()"

class DictPTMPosManager(DictDictManager):
    method = "as_dict_ptmpos()"

class Atom(models.Model):
    """ """
    symbol = models.CharField(verbose_name = "Symbol",
                              max_length = 10,
                              unique = True)
    name = models.CharField(verbose_name = "Name",
                            max_length = 255,
                            unique = True)
    
    def list_wanted_fields(self):
        return ["symbol", "name"]
    
    def detail_wanted_fields(self):
        return ["symbol", "name"]
        
    def sorted_wanted_fields(self):
        return ["symbol", "name"]
    
    def __unicode__(self):
        return self.name
        
class IsoAtom(models.Model):
    """ """
    atom = models.ForeignKey(Atom, verbose_name = "Atom")
    isotop = models.SmallIntegerField(verbose_name = "Mass Number")
    abound = models.FloatField(verbose_name = "Abound")
    mass = models.FloatField(verbose_name = "Mass in Dalton")
    objects = models.Manager()
    dict_objects = DictDictManager()
    
    
    def list_wanted_fields(self):
        return ["atom", "isotop", "abound", "mass"]
   
    def detail_wanted_fields(self):
        return ["atom", "isotop", "abound", "mass"]
    
    def sorted_wanted_fields(self):
        return ["atom", "isotop", "abound", "mass"]
    
    def as_dict(self):
        d = {}
        d[self.atom.symbol] = [(self.mass, self.abound, self.isotop)]
        return d
    
    def __unicode__(self):
        return self.atom.__str__()+str(self.isotop)

class AtomNB(models.Model):
    """ """
    atom = models.ForeignKey(Atom, verbose_name = "Atom")
    nb_occurence = models.SmallIntegerField(verbose_name = "Occurences")
    objects = models.Manager()
    dict_objects = DictDictManager()
    
    def as_dict(self):
        d = {}
        d[self.atom.symbol] = self.nb_occurence
        return d
    
    def __unicode__(self):
        return self.atom.symbol+str(self.nb_occurence)
    
class AminoAcid(models.Model):
    """ """
    symbol = models.CharField(verbose_name = "Symbol",
                              max_length = 10,
                              unique = True)
    name = models.CharField(verbose_name = "Name",
                            max_length = 255,
                            unique = True)
    atoms = models.ManyToManyField(AtomNB, verbose_name = "Composition")
    objects = models.Manager()
    dict_objects = DictDictManager()

    
    def list_wanted_fields(self):
        return ["symbol", "name", "atoms"]
        
    def detail_wanted_fields(self):
        return ["symbol", "name", "atoms"]
    
    def sorted_wanted_fields(self):
        return ["symbol", "name"]
    
    def as_dict(self):
        d = {}
        compo = {}
        
        for atom in self.atoms.all():
            #~ print >> sys.stderr, atom.as_dict()
            compo.update(atom.as_dict())
        #~ for atom in self.atoms:
            #~ compo.update(atom.as_dict())
        d[self.symbol] = compo
        return d
    
    def __unicode__(self):
        return self.name

class Protease(models.Model):
    """ """
    name = models.CharField(verbose_name = "Name",
                            max_length = 255,
                            unique = True)
    cleave = models.ManyToManyField(AminoAcid, verbose_name = "Cleave")
    objects = models.Manager()
    dict_objects = DictDictManager()
    
    def list_wanted_fields(self):
        return ["name", "cleave"]
    
    def detail_wanted_fields(self):
        return ["name", "cleave"]
    
    def sorted_wanted_fields(self):
        return ["name"]
    
    def as_dict(self):
        d = {}
        aas = []
        for aa in self.cleave.all():
            aas.append(aa.symbol)
        d[self.name] = aas
        return d
    
    def __unicode__(self):
        return self.name

class Link(models.Model):
    """ """
    name = models.CharField(verbose_name = "Name",
                            max_length = 255,
                            unique = True)
    atoms = models.ManyToManyField(AtomNB, related_name="%(class)s_atoms", 
                                   verbose_name = "Composition", blank=True)
    unprotatoms = models.ManyToManyField(AtomNB, 
                                         related_name="%(class)s_unprotatoms",
                                         verbose_name = "Unprotenated Composition", blank=True)
    complementary = models.ForeignKey('self', null=True, blank=True, verbose_name = "Complementary")
    opposite = models.ManyToManyField('self', blank=True, verbose_name = "Opposites")
    objects = models.Manager()
    dict_objects = DictListManager()
    
    def list_wanted_fields(self):
        return ["name", "complementary", "atoms", "unprotatoms", "opposite"]
    
    def detail_wanted_fields(self):
        return ["name", "atoms", "unprotatoms", "complementary", "opposite"]
    
    def sorted_wanted_fields(self):
        return ["name", "complementary"]
    
    def as_dict(self):
        d = {}
        d["name"] = self.name
        composition = {}
        unprotcompo = {}
        opposites = []
        if self.complementary != None:
            d["complementary"] = self.complementary.name
        else:
            d["complementary"] = None
        for atomnb in self.atoms.all():
            composition.update(atomnb.as_dict())
        for atomnb in self.unprotatoms.all():
            unprotcompo.update(atomnb.as_dict())
        for oplink in self.opposite.all():
            opposites.append(oplink.name)
        d["composition"] = composition
        d["unprotcompo"] = unprotcompo
        d["opposite"] = opposites
        return d
    
    def __unicode__(self):
        return self.name

class Position(models.Model):
    """ """
    name = models.CharField(verbose_name = "Name",
                            max_length = 255)
    amino_acid = models.ForeignKey(AminoAcid,
                                   verbose_name = "Amino Acid")
    
    def __unicode__(self):
        return self.name+self.amino_acid.symbol

class PTM(models.Model):
    """ """
    name = models.CharField(verbose_name = "Name",
                            max_length = 255,
                            unique = True)
    atoms = models.ManyToManyField(AtomNB, verbose_name = "Composition")
    position = models.ManyToManyField(Position, verbose_name = "Positions")
    objects = models.Manager()
    dict_objects = DictPTMManager()
    dict_objects_pos = DictPTMPosManager()

    def list_wanted_fields(self):
        return ["name", "atoms", "position"]
    
    def detail_wanted_fields(self):
        return ["name", "atoms", "position"]
    
    def sorted_wanted_fields(self):
        return ["name"]
    
    def as_dict_ptm(self):
        d = {}
        composition = {}
        for atomnb in self.atoms.all():
            composition.update(atomnb.as_dict())
        d[self.name] = composition
        return d
    
    def as_dict_ptmpos(self):
        d = {} 
        posdict = {}
        for pos in self.position.all():
            edge = posdict.get(pos.amino_acid.symbol, [])
            edge.append(pos.name)
            posdict[pos.amino_acid.symbol] = edge
        d[self.name] = posdict
        return d
    
    def __unicode__(self):
        return self.name

class SmallResidue(models.Model):
    """this class represent smallresidues (for exemple immonium ions)
    it has a name, a composition in atom and a PTMPos which give
    a ptm, an amino acid and a position(n-term, c-term, center) to 
    know on what kind of amino acid and ptm the small residue could 
    comes 
    a db table with relations:
    A name
    A N relation to AtomNB class/table to represent a composition
    A N relation to PTMPos class/table
    """
    name = models.CharField(verbose_name = "Name",
                            max_length = 255,
                            unique = True)
    atoms = models.ManyToManyField(AtomNB, verbose_name = "Composition")
    
    def list_wanted_fields(self):
        return ["name", "atoms"]
    
    def detail_wanted_fields(self):
        return ["name", "atoms"]
    
    def sorted_wanted_fields(self):
        return ["name"]
    
    def as_dict(self):
        d = {}
        compo = {}
        for atom in self.atoms.all():
            #~ print >> sys.stderr, atom.as_dict()
            compo.update(atom.as_dict())
        #~ for atom in self.atoms:
            #~ compo.update(atom.as_dict())
        d[self.name] = compo
        return d
    
    def __unicode__(self):
        return self.name


class Import(models.Model):
    """ """
    CHIMICAL_CHOICES = (
        ('Atom', 'Atoms'),
        ('IsoAtom', 'Isotopes'),
        ('AminoAcidNames', 'Amino Acids Names'),
        ('AminoAcidCompo', 'Amino Acids Compositions'),
        ('Protease', 'Proteases'),
        ('LinkCompo', 'Links Composition'),
        ('LinkUnprot', 'Links Unprot Composition'),
        ('LinkRelation', 'Links Relations'),
        ('PTMCompo', 'PTMs Composition'),
        ('PTMPosition', 'PTMs Position'),
        ('SmallResidue', 'Small Residues')
    )
    
    import_type = models.CharField(verbose_name="Import Type",
                                   max_length=100, 
                                   choices=CHIMICAL_CHOICES,
                                   default='Atom')
    csvfile = models.FileField(verbose_name = "CSV File", upload_to='uploads')
    
    def __str__(self):
        return self.import_type
    
