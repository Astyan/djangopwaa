from django import forms
from chimicals.models import Import

class ImportChimicalsForm(forms.ModelForm):
    
    class Meta:
        model = Import
        fields = ['import_type', 'csvfile']

    def __init__(self, *args, **kwargs):
        """ """
        super(ImportChimicalsForm, self).__init__(*args, **kwargs)
        self.fields['import_type'].widget.attrs.update({'class' : 'form-control'})
