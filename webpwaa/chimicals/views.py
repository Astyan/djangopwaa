from django.shortcuts import render, get_object_or_404
from django.views.generic.list import ListView
from django.views.generic import DetailView, CreateView
from collections import OrderedDict
from django.core.urlresolvers import reverse_lazy

from chimicals.models import (Atom, IsoAtom, AminoAcid, Protease,
                              Link, PTM, SmallResidue, Import)
from chimicals.form import ImportChimicalsForm
from chimicals.insertdata import (InsertAtoms, InsertIsoAtoms, 
                                  InsertAminoAcid, InsertProtease, 
                                  InsertLink, InsertPTM, 
                                  InsertResidue)
from webpwaa.viewparents import WebPWAAListView, WebPWAADetailView

class ChimicalsDetailView(WebPWAADetailView):
    template_name = 'chimicals/default_detail.html'
    
    def get_context_data(self, **kwargs):
        context = super(ChimicalsDetailView, self).get_context_data(**kwargs)
        context["delete"] = "admin:chimicals_" + self.model_name + "_delete"
        context["change"] = "admin:chimicals_" + self.model_name + "_change"
        return context

#LIST VIEW

class AtomListView(WebPWAAListView):
    model = Atom
    name = "Atoms"
    template_name = 'chimicals/default_list.html'
    default_order_by = "name"
    detailurl = "atom_detail"
    pageurl = "atom_list"

class IsoAtomListView(WebPWAAListView):
    model = IsoAtom
    name = "Isotopes"
    template_name = 'chimicals/default_list.html'
    default_order_by = "atom"
    detailurl = "isoatom_detail"
    pageurl = "isoatom_list"
    
class AminoAcidListView(WebPWAAListView):
    model = AminoAcid
    name = "Amino Acids"
    template_name = 'chimicals/default_list.html'
    default_order_by = "name"
    detailurl = "aminoacid_detail"
    pageurl = "aminoacid_list"
    
class ProteaseListView(WebPWAAListView):
    model = Protease
    name = "Proteases"
    template_name = 'chimicals/default_list.html'
    default_order_by = "name"
    detailurl = "protease_detail"
    pageurl = "protease_list"

class LinkListView(WebPWAAListView):
    model = Link
    name = "Links"
    template_name = 'chimicals/default_list.html'
    default_order_by = "name"
    detailurl = "link_detail"
    pageurl = "link_list"

class PTMListView(WebPWAAListView):
    model = PTM
    name = "PTMs"
    template_name = 'chimicals/default_list.html'
    default_order_by = "name"
    detailurl = "ptm_detail"
    pageurl = "ptm_list"
    
class SmallResidueListView(WebPWAAListView):
    model = SmallResidue
    name = "Small Residues"
    template_name = 'chimicals/default_list.html'
    default_order_by = "name"
    detailurl = "residue_detail"
    pageurl = "residue_list"


#DETAIL VIEW

class AtomDetailView(ChimicalsDetailView):
    model = Atom
    model_name = "atom"
    name = "Atom"
        
class IsoAtomDetailView(ChimicalsDetailView):
    model = IsoAtom
    model_name = "isoatom"
    name = "Isotope" 

class AminoAcidDetailView(ChimicalsDetailView):
    model = AminoAcid
    model_name = "aminoacid"
    name = "Amino Acid"

class ProteaseDetailView(ChimicalsDetailView):
    model = Protease
    name = "Protease"
    model_name = "protease"
    
class LinkDetailView(ChimicalsDetailView):
    model = Link
    name = "Link"
    model_name = "link"
    
class PTMDetailView(ChimicalsDetailView):
    model = PTM
    name = "PTM"
    model_name = "ptm"

class PTMDetailView(ChimicalsDetailView):
    model = PTM
    name = "PTM"
    model_name = "ptm"    
    
class SmallResidueDetailView(ChimicalsDetailView):
    model = SmallResidue
    name = "Small Residue"
    model_name = "smallresidue"    

#~ def importchimicalsview(request):
    #~ if request.method == 'POST':
        #~ form = ImportChimicalsForm(request.POST)
        #~ print form.is_valid()
        #~ 
        #~ if form.is_valid():
            #~ import_type = form.cleaned_data['import_type']
            #~ import_file = form.cleaned_data['import_file']
            #~ 
            #~ print import_type, import_file
    #~ 
    #~ else:
        #~ form = ImportChimicalsForm
    #~ return render(request, 'chimicals/import.html', locals())

class ImportChimicalsView(CreateView):
    model = Import
    template_name = 'chimicals/import.html'
    form_class = ImportChimicalsForm
    form_url = 'import_chimicals'
    name = "Import CSV"
    success_url=reverse_lazy('chimicals_index')
        
    def form_valid(self, form):
        usefull_args = self.get_form_kwargs()
        #~ open don't work 
        #~ print usefull_args
        openedfile = usefull_args["files"]["csvfile"]
        #~ for truc in csv.DictReader(openfile, delimiter=b','): 
            #~ print truc
        #~ do the thing with usefull args
        actionname = usefull_args["data"]["import_type"]
        #save in databse the csv file data
        if actionname == "Atom": 
            ia = InsertAtoms(openedfile)
            ia.insertall()
        elif actionname == "IsoAtom":
            iia = InsertIsoAtoms(openedfile)
            iia.insertall()
        elif actionname == "AminoAcidNames": 
            iaa = InsertAminoAcid(fcsvaaname=openedfile)
            iaa.insertallaa()
        elif actionname == "AminoAcidCompo": 
            iaa = InsertAminoAcid(fcsvaacomposition=openedfile)
            iaa.insertallaatoms()
        elif actionname == "Protease": 
            ip = InsertProtease(openedfile)
            ip.insertall()
        elif actionname == "PTMCompo": 
            ip = InsertPTM(fcsvptm=openedfile)
            ip.insert_all_ptm()
        elif actionname == "PTMPosition": 
            ip = InsertPTM(fcsvptmpos=openedfile)
            ip.insert_all_ptmpos()
        elif actionname == "LinkCompo": 
            il = InsertLink(fcsvcompo=openedfile)
            il.insert_namecompo()
        elif actionname == "LinkUnprot":
            il = InsertLink(fcsvuprotcompo=openedfile)
            il.insert_unprot()
        elif actionname == "LinkRelation":
            il = InsertLink(fcsvrelation=openedfile)
            il.insert_otherlinks()
        elif actionname == "SmallResidue":
            sr = InsertResidue(openedfile)
            sr.insertall()
        return super(ImportChimicalsView, self).form_valid(form)
            
    def get_context_data(self, **kwargs):
        context = super(ImportChimicalsView, self).get_context_data(**kwargs)
        context['formurl'] = self.form_url
        return context
    
