import os, csv, sys
from chimicals.models import (Atom, IsoAtom, AtomNB, AminoAcid,
                              Protease, Link, Position, 
                              PTM, SmallResidue)
                              

class InsertAtoms(object):
    
    def __init__(self, fcsv):
        """Open the csv file and extract Atoms informations
        @param fcsv: the path to the csv file
        @type fcsv: string"""
        self.atomdict = {}
        #~ if fcsv == None:
            #~ fcsv = os.path.join(".", "files", "atoms.csv")
        atomsfile = fcsv
        for atom in csv.DictReader(atomsfile, delimiter=b','):
            self.atomdict[atom['symbol']] = atom['name']
        #~ atomsfile.close()
    
    def insertall(self):
        for symbol in self.atomdict:
            atom = Atom(symbol=symbol, name=self.atomdict[symbol])
            atom.save()

class InsertIsoAtoms(object):
    """The class is used to get atoms isotop from the csv file and 
    save it in the database """ 
    
    def __init__(self, fcsv=None):
        """Open the csv file and extract Isotop informations
        @param fcsv: the path to the csv file
        @type fcsv: string"""
        self.isodict = {}
        #~ if fcsv == None:
            #~ fcsv = os.path.join(".", "files", "isotops.csv")
        isosfile = fcsv
        for iso in csv.DictReader(isosfile, delimiter=b','):
            self.isodict[iso['symbol']] = []
        #~ isosfile.close()
        #need another opening 
        isosfile = fcsv
        for iso in csv.DictReader(isosfile, delimiter=b','):
            self.isodict[iso['symbol']].append({"isotop":iso['iso'],
                                                "mass":iso['mass'], 
                                                "abound":iso['abound']})
        #~ isosfile.close()

    def insertall(self):
        """insert all isotops in the database"""
        for atom in self.isodict:
            for isotop in self.isodict[atom]:
                at = Atom.objects.get(symbol=atom)
                isoatom = IsoAtom(atom=at, isotop=isotop["isotop"],
                                  mass=isotop["mass"], 
                                  abound=isotop["abound"])
                isoatom.save()

class InsertAminoAcid(object):
    """The class is used to get amino acid from the csv file and 
    save it in the database """ 
    
    def __init__(self, fcsvaaname=None, fcsvaacomposition=None):
        """Open the csv file and extract Atoms informations
        @param fcsvaaname: the path to the csv of aa symbol with name
        @type fcsvaaname: string
        @param fcsvaacomposition: the path to the csv of aa symbol with 
        composition
        @type fcsvaacomposition: string"""
        self.aanamedict = {}
        self.aacompodict = {}
        #~ if fcsvaaname == None:
            #~ fcsvaaname = os.path.join(".", "files", "aaname.csv")
        #~ if fcsvaacomposition == None:
            #~ fcsvaacomposition = \
            #~ os.path.join(".", "files", "aacomposition.csv")
        #store aa symbol and name
        aasfile = fcsvaaname
        if aasfile != None:
            for aa in csv.DictReader(aasfile, delimiter=b','):
                self.aanamedict[aa['symbol']] = aa['name']
            #~ aasfile.close()
        #store aa composition
        aasfile = fcsvaacomposition
        if aasfile != None:
            for aa in csv.DictReader(aasfile, delimiter=b','):
                self.aacompodict[aa['symbol']] = {}
                for key in aa:
                    if key != 'symbol':
                        self.aacompodict[aa['symbol']][key] = aa[key]
            #~ aasfile.close()

    def insertallaa(self):
        """insert all amino acids in the database"""
        for symbol in self.aanamedict:
            aa = AminoAcid(symbol=symbol, name=self.aanamedict[symbol])
            aa.save()
        
    def insertallaatoms(self):
        """insert all atoms for each amino acid in the bdd"""
        for amino_acid in self.aacompodict:
            for atom in self.aacompodict[amino_acid]:
                at = Atom.objects.get(symbol=atom)
                if self.aacompodict[amino_acid][atom] != 0:
                    atomnb, create = AtomNB.objects.get_or_create(atom=at,
                                                                  nb_occurence=self.aacompodict
                                                                  [amino_acid][atom])
                    if create:
                        atomnb.save()
                    aa = AminoAcid.objects.get(symbol=amino_acid)
                    aa.atoms.add(atomnb)
                    aa.save()
        
    def insertall(self):
        """insertall all amino acid and there composition"""
        self.insertallaa()
        self.insertallaatoms()


class InsertProtease(object):
    """The class is used to get protease from the csv file and 
    save it in the database """ 
    
    def __init__(self, fcsv=None):
        """Open the csv file and extract Proteases informations
        @param fcsv: the path to the csv file
        @type fcsv: string"""
        self.proteaseslist = []
        self.proteasesdict = {}
        #~ if fcsv == None:
            #~ fcsv = os.path.join(".", "files", "proteases.csv")
        proteasesfile = fcsv
        for protease in csv.DictReader(proteasesfile, delimiter=b','):
            self.proteasesdict = {}
            self.proteasesdict[protease['protease']] = \
            str(protease['cleave'])
            self.proteaseslist.append(self.proteasesdict)
        #~ proteasesfile.close()
    
    
    def insertall(self):
        """insert all proteases in the database"""
        for protease in self.proteaseslist:
            for pname in protease:
                prot, created = Protease.objects.get_or_create(name=pname)
                
                for aa in protease[pname]:
                    aares = AminoAcid.objects.get(symbol=aa)
                    prot.cleave.add(aares)
                prot.save()
                
#Link in two run : first : set the name the composition
#second the other link

class InsertLink(object):
    """The class is used to get links from the csv file and 
    save it in the database """ 
    
    def __init__(self, fcsvcompo=None, fcsvrelation=None, 
                 fcsvuprotcompo=None):
        """Open the csv file and extract Links informations
        @param fcsvcompo: the path to the csv file with the composition
        in amino acid
        @type fcsvcompo: string
        @param fcsvrelation: the path to the csv file with relation
        between links
        @type fcsvrelation: string
        @param fcsvuprotcompo: the path to the csv file with the
        unprotoned composition in amino acid"""
        self.linkcompodict = {}
        self.linkunprotcompodict = {}
        self.linkrelationdict = {}
        #~ if fcsvuprotcompo == None:
            #~ fcsvuprotcompo = os.path.join(".", "files", 
                                          #~ "unprotlinkscompo.csv")
        #~ if fcsvcompo == None:
            #~ fcsvcompo = os.path.join(".", "files", "linkscompo.csv")
        #~ if fcsvrelation == None:
            #~ fcsvrelation = os.path.join(".", "files", 
                                        #~ "linksrelation.csv")
        #save the compo
        linksfile = fcsvcompo
        if linksfile != None:
            for link in csv.DictReader(linksfile, delimiter=b','):
                self.linkcompodict[link['link']] = {}
                for key in link:
                    if key != 'link':
                        self.linkcompodict[link['link']][key] = link[key]
                        self.linkrelationdict[link['link']] = {}
        #save unprot compo
        linksfile = fcsvuprotcompo
        if linksfile != None:
            for link in csv.DictReader(linksfile, delimiter=b','):
                self.linkunprotcompodict[link['link']] = {}
                for key in link:
                    if key != 'link':
                        self.linkunprotcompodict[link['link']][key] = \
                        link[key]
        #save relation
        linksfile = fcsvrelation
        if linksfile != None:
            for relation in csv.DictReader(linksfile, delimiter=b','):
                for key in relation:
                    if key != 'link':
                        #just set
                        setrel = self.linkrelationdict.get(relation['link'], {})
                        self.linkrelationdict[relation['link']] = setrel
                        #~ print self.linkrelationdict
                        #~ datatmp = self.linkrelationdict.get([relation['link']], {})
                        if key == "complementary":
                            data = relation[key]
                        else:
                            data = self.linkrelationdict[relation['link']].get(key, [])
                            data.append(relation[key])
                        self.linkrelationdict[relation['link']][key] = data

    def insert_namecompo(self):
        for link in self.linkcompodict:
            lcur = Link(name=link)
            lcur.save()
            for atom in self.linkcompodict[link]:
                at = Atom.objects.get(symbol=atom)
                anb, created = AtomNB.objects.get_or_create(atom=at, nb_occurence=self.linkcompodict[link][atom])
                if created:
                    anb.save()
                lcur.atoms.add(anb)
    
    def insert_unprot(self):
        for link in self.linkunprotcompodict:
            lcur, created = Link.objects.get_or_create(name=link)
            if created:
                lcur.save()
            for atom in self.linkunprotcompodict[link]:
                at = Atom.objects.get(symbol=atom)
                anb, create = AtomNB.objects.get_or_create(atom=at, nb_occurence=self.linkunprotcompodict[link][atom])
                if create:
                    anb.save()
                lcur.unprotatoms.add(anb)
            lcur.save()
                
                
    def insert_otherlinks(self):
        #~ print >> sys.stderr, self.linkrelationdict
        for link in self.linkrelationdict:
            lcur = Link.objects.get(name=link)
            for relation in self.linkrelationdict[link]:
                if relation == "complementary":
                    if self.linkrelationdict[link][relation] != "None":
                        lrel = Link.objects.get(name=self.linkrelationdict[link][relation])
                        lcur.complementary = lrel
                else:
                    for lop in self.linkrelationdict[link][relation]:
                        lrel = Link.objects.get(name=lop)
                        lcur.opposite.add(lrel)
            lcur.save()
            
    def insertall(self):
        """insert all links in the database"""
        self.insert_namecompo()
        self.insert_unprot()
        self.insert_otherlinks()

class InsertPTM(object):
    """The class is used to get ptms from the csv file and 
    save it in the database """ 
    
    def __init__(self, fcsvptm=None, fcsvptmpos=None):
        """Open the csv file and extract Links informations
        @param fcsvptm: the path to the csv ptm file
        @type fcsvptm: string
        @param fcsvptmpos: the path to the csv ptmpos file
        @type fcsvptmpos: string"""
        self.ptmsdict = {}
        self.ptmsposlist = []
        #~ if fcsvptm == None:
            #~ fcsvptm = os.path.join(".", "files", "ptms.csv")
        #~ if fcsvptmpos == None:
            #~ fcsvptmpos = os.path.join(".", "files", "ptmspos.csv")
        ptmsfile = fcsvptm
        if ptmsfile != None:
            for ptm in csv.DictReader(ptmsfile, delimiter=b','):
                self.ptmsdict[ptm['name']] = {}
                for key in ptm:
                    if key != 'name':
                        self.ptmsdict[ptm['name']][key] = ptm[key]
            #~ ptmsfile.close()
        if fcsvptmpos != None:
            ptmsposfile = fcsvptmpos
            for ptmpos in csv.DictReader(ptmsposfile, delimiter=b','):
                self.ptmsposlist.append(ptmpos)
            ptmsposfile.close()
    
    def insert_all_ptm(self):
        """insert all ptm in the database"""
        for ptm in self.ptmsdict:
            ptmcur = PTM(name=ptm)
            ptmcur.save()
            for atom in self.ptmsdict[ptm]:
                if self.ptmsdict[ptm][atom] != 0:
                    at = Atom.objects.get(symbol=atom)
                    anb, created = AtomNB.objects.get_or_create(atom=at, nb_occurence=self.ptmsdict[ptm][atom])
                    if created:
                        anb.save()
                    ptmcur.atoms.add(anb)
            ptmcur.save()
    
    def insert_all_ptmpos(self):
        """ insert all ptmpos in the database"""
        for ptm in self.ptmsposlist:
            ptmcur = PTM.objects.get(name=ptm['ptm'])
            aa = AminoAcid.objects.get(symbol=ptm['aa'])
            pos, created = Position.objects.get_or_create(name=ptm['position'], amino_acid=aa)
            if created:
                pos.save()
            ptmcur.position.add(pos)
            ptmcur.save()
    
    def insertall(self):
        """insert all ptms and ptmspos in the database """
        self.insert_all_ptm()
        self.insert_all_ptmpos()

class InsertResidue(object):
    
    def __init__(self, fcsv):
        """Open the csv file and extract Atoms informations
        @param fcsv: the path to the csv file
        @type fcsv: string"""
        self.residuesdict = {}
        residues = fcsv
        for res in csv.DictReader(residues, delimiter=b','):
            self.residuesdict[res['name']] = {}
            for key in res:
                if key != 'name':
                    self.residuesdict[res['name']][key] = res[key]
            
            
    def insertall(self):
        """insert all atoms for each amino acid in the bdd"""
        #~ print "lol"
        for residue in self.residuesdict:
            for atom in self.residuesdict[residue]:
                at = Atom.objects.get(symbol=atom)
                if self.residuesdict[residue][atom] != 0:
                    atomnb, create = AtomNB.objects.get_or_create(atom=at,
                                                                  nb_occurence=self.residuesdict
                                                                  [residue][atom])
                    if create:
                        atomnb.save()
                    smallresidue, create = SmallResidue.objects.get_or_create(name=residue)
                    smallresidue.atoms.add(atomnb)
                    smallresidue.save()
    

if __name__ == "__main__":
    pass
    #~ ia = InsertAtoms("chimicals/files/atoms.csv")
    #~ ia.insertall()
    #~ iia = InsertIsoAtoms("chimicals/files/isotops.csv")
    #~ iia.insertall()
    #~ aa = InsertAminoAcid("chimicals/files/aaname.csv", "chimicals/files/aacomposition.csv")
    #~ aa.insertall()
    #~ prot = InsertProtease("chimicals/files/proteases.csv")
    #~ prot.insertall()
    #~ link = InsertLink("chimicals/files/linkscompo.csv",
                      #~ "chimicals/files/linksrelation.csv",
                      #~ "chimicals/files/unprotlinkscompo.csv")
    #~ link.insertall()
    #~ ptm = InsertPTM("chimicals/files/ptms.csv", 
                    #~ "chimicals/files/ptmspos.csv")
    #~ ptm.insertall()
    #~ print >> sys.stderr, IsoAtom.dict_objects.all()
    #~ print >> sys.stderr, AminoAcid.dict_objects.all()
    #~ print >> sys.stderr, Protease.dict_objects.all()
    #~ print >> sys.stderr, Link.dict_objects.all()
    #~ print >> sys.stderr, PTM.dict_objects.all()
    #~ print >> sys.stderr, PTM.dict_objects_pos.all()
