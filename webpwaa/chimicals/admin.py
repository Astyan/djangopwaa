from django.contrib import admin

# Register your models here.
from chimicals.models import (Atom, IsoAtom, AtomNB, Protease, 
                              AminoAcid, Link, PTM, Position, 
                              SmallResidue, Import)

class AtomAdmin(admin.ModelAdmin):
    list_display = ('symbol', 'name')
    list_filter = ('symbol', 'name')
    ordering = ('symbol', 'name')
    search_fields = ('symbol', 'name')
    
class IsoAtomAdmin(admin.ModelAdmin):
    list_display = ('atom', 'isotop', 'abound', 'mass')
    list_filter = ('atom', 'isotop', 'abound', 'mass')
    ordering = ('atom', 'isotop', 'abound', 'mass')
    search_fields = ('atom', 'isotop', 'abound', 'mass')
    
class AtomNBAdmin(admin.ModelAdmin):
    list_display = ('atom', 'nb_occurence')
    list_filter = ('atom', 'nb_occurence')
    ordering = ('atom', 'nb_occurence')
    search_fields = ('atom', 'nb_occurence')

class NameAdmin(admin.ModelAdmin):
    list_display = ['name']
    list_filter = ['name']
    ordering = ['name']
    search_fields = ['name']

class AminoAcidAdmin(admin.ModelAdmin):
    list_display = ('symbol', 'name')
    list_filter = ('symbol', 'name')
    ordering = ('symbol', 'name')
    search_fields = ('symbol', 'name')

class PositionAdmin(admin.ModelAdmin):
    list_display = ('name', 'amino_acid')
    list_filter = ('name', 'amino_acid')
    ordering = ('name', 'amino_acid')
    search_fields = ('name', 'amino_acid')

class SmallResidueAdmin(admin.ModelAdmin):
    list_display = ['name']
    list_filter = ['name']
    ordering = ['name']
    search_fields = ['name']


admin.site.register(Atom, AtomAdmin)
admin.site.register(IsoAtom, IsoAtomAdmin)
admin.site.register(AtomNB, AtomNBAdmin)
admin.site.register(Protease, NameAdmin)
admin.site.register(AminoAcid, AminoAcidAdmin)
admin.site.register(Link, NameAdmin)
admin.site.register(PTM, NameAdmin)
admin.site.register(Position, PositionAdmin)
admin.site.register(SmallResidue, SmallResidueAdmin)
admin.site.register(Import)
