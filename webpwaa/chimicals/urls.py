from django.conf.urls import patterns, url
from django.views.generic import TemplateView

#~ from . import views
from chimicals.views import (AtomListView, AtomDetailView, 
                             IsoAtomListView, IsoAtomDetailView,
                             AminoAcidListView, AminoAcidDetailView,
                             ProteaseListView, ProteaseDetailView,
                             LinkListView, LinkDetailView,
                             PTMListView, PTMDetailView,
                             SmallResidueListView, SmallResidueDetailView,
                             ImportChimicalsView)
#~ from chimicals.models import Atom, Iso



#~ urlpatterns = [ url(r'^atoms$', views.show_atoms, name="atoms"),
                #~ url(r'^atoms/(?P<id>\d+)$', views.show_atom, name="atom"),
#~ ]


urlpatterns = [ url(r'^$', TemplateView.as_view(template_name="chimicals_base.html"),
                                                name="chimicals_index"),
                url(r'^atoms$', AtomListView.as_view(),
                    name="atom_list"),
                url(r'^atoms/(?P<pk>\d+)$', AtomDetailView.as_view(),
                    name="atom_detail"),
                url(r'^isotope$', IsoAtomListView.as_view(),
                    name="isoatom_list"),
                url(r'^isotope/(?P<pk>\d+)$', IsoAtomDetailView.as_view(),
                    name="isoatom_detail"),  
                url(r'^aminoacid$', AminoAcidListView.as_view(),
                    name="aminoacid_list"),
                url(r'^aminoacid/(?P<pk>\d+)$', AminoAcidDetailView.as_view(),
                    name="aminoacid_detail"), 
                url(r'^protease$', ProteaseListView.as_view(),
                    name="protease_list"),
                url(r'^protease/(?P<pk>\d+)$', ProteaseDetailView.as_view(),
                    name="protease_detail"),
                url(r'^link$', LinkListView.as_view(),
                    name="link_list"),
                url(r'^link/(?P<pk>\d+)$', LinkDetailView.as_view(),
                    name="link_detail"),
                url(r'^ptm$', PTMListView.as_view(),
                    name="ptm_list"),
                url(r'^ptm/(?P<pk>\d+)$', PTMDetailView.as_view(),
                    name="ptm_detail"),
                url(r'^residue$', SmallResidueListView.as_view(),
                    name="residue_list"),
                url(r'^residue/(?P<pk>\d+)$', SmallResidueDetailView.as_view(),
                    name="residue_detail"),
                url(r'^import$', ImportChimicalsView.as_view(), 
                    name='import_chimicals'),
]
